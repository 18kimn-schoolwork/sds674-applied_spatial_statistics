#contextual variables to be used with the property data
library(tidyverse)
library(tidycensus)
library(sf)
library(tigris)
vars <- load_variables(2018, "acs5", cache = T)
acs_vars <- c(paste0("B19301", c("B","H"), "_001"), 
              paste0("B28009B", c("_001", "_004")),
              paste0("B28009H", c("_001", "_004")), 
              paste0("C24010H", c("_003", "_005", "_002", "_008", "_009", "_011")),
              paste0("C24010B", c("_003", "_005", "_002", "_008", "_009", "_011")), 
              paste0("B14007H", c("_017", "_018", "_001")),
              paste0("B14007B", c("_017", "_018", "_001")))


context_data <- get_acs(geography = "block group",variables =  acs_vars, 
                      year = 2018,
                      county = "New Haven", state = "CT")  %>% 
  left_join(vars, by = c( "variable" = "name")) %>% 
  select(-moe, -NAME, -concept, -label) %>% 
  mutate(race = case_when(str_detect(variable, "B_") ~ "B",
                          str_detect(variable, "H_") ~ "W"))

#for educational attainment, two vars of prop. high school diploma holders and college graduates (bachelor's + )
#for mobility, prop. moved
#for rent, prop. renting
#for health insurance, prop. without health insurance
races <- c("B","W")
no_broadband <- map_dfr(races, function(race){
  sub <- context_data %>% 
    filter(race == !!race, str_detect(variable, "B28009")) %>%
    pivot_wider(names_from = variable, values_from = estimate)
  internet <- str_subset(names(sub), "004")
  tot <- str_subset(names(sub), "001")
  sub <- sub %>% 
    mutate(no_broadband := 1 - !!sym(internet)/!!sym(tot)) %>% 
    select(GEOID, race, no_broadband)
  return(sub)
})

white_collar <- map_dfr(races, function(race){
  sub <- context_data %>% 
    filter(race == !!race, str_detect(variable, "C24010")) %>%
    pivot_wider(names_from = variable, values_from = estimate)
  male <- str_subset(names(sub), "_002")
  male2 <- str_subset(names(sub), "_003")
  male3 <- str_subset(names(sub), "_005")
  female <- str_subset(names(sub), "_008")
  female2 <- str_subset(names(sub), "_009")
  female3 <- str_subset(names(sub), "_011")

  sub <- sub %>% mutate(white_collar := (!!sym(male2) + !!sym(male3) + 
                          !!sym(female2) + !!sym(female3))/
                          (!!sym(male) + !!sym(female))) %>% 
    select(GEOID, race, white_collar)
})

in_uni <- map_dfr(races, function(race){
  sub <- context_data %>% 
    filter(race == !!race, str_detect(variable, "B14007")) %>%
    pivot_wider(names_from = variable, values_from = estimate)
  tot <- str_subset(names(sub), "_001")
  cat1 <- str_subset(names(sub), "_017")
  cat2 <- str_subset(names(sub), "_018")

  
  sub <- sub %>% mutate(in_uni := (!!sym(cat1) + !!sym(cat2))/
                          (!!sym(tot))) %>% 
    select(GEOID, race, in_uni)
})



census_varstbl <- load_variables(2010, "sf1", cache = T)
census_vars <- c("P007001", "P007003", "P007004", #total, white,black
                 "H014003","H014011", #owner/renter white
                 "H014004", "H014012", #owner/renter black
                 "H012I001","H012B001") #avg household size, white / black

census_data <- get_decennial(geography = "block group",variables =  census_vars, 
                        county = "New Haven", state = "CT")  %>% 
  left_join(census_varstbl, by = c( "variable" = "name")) %>% 
  select(-NAME, -concept, -label) %>% 
  pivot_wider(names_from = variable) %>% 
  mutate(B_tot = P007004, 
         W_tot = P007003, 
         B_prop = P007004/P007001,
         W_prop = P007003/P007001, 
         W_renting = H014011/(H014011+H014003), 
         B_renting = H014012/(H014012+H014004),
         B_hhsize = H012B001, 
         W_hhsize = H012I001
         ) %>% 
  select(GEOID, B_tot:W_hhsize) %>% 
  pivot_longer(B_tot:W_hhsize,
               names_sep = "_", 
               names_to = c("race", "var")) %>% 
  pivot_wider(names_from = var)


list(white_collar, in_uni, no_broadband, census_data) %>% 
  reduce(full_join, by = c("GEOID", "race")) %>% 
  mutate_at(vars(white_collar:no_broadband, prop:hhsize), function(x) 100*x) %>% 
  write_csv("final_proj/data/contextual variables.csv")

block_groups("09", "009") %>% 
  st_as_sf() %>% 
  select(GEOID) %>% 
  saveRDS("final_proj/data/nhv_blockgroups_shapefile.RDS")


 