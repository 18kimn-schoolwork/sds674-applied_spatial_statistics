# "Tissot's Indicatrix as a Measure of Projection Deformity"	
# "HBG"	
# "January 23, 2018"	
	
	
# #### Tissot's Indicatrix - What is it?	
	
# Modified from [Wikipedia](https://en.wikipedia.org/wiki/Tissot%27s_indicatrix): "In cartography, a Tissot's indicatrix (aka Tissot's ellipse, Tissot ellipse, ellipse of distortion) is a mathematical contrivance presented by French mathematician Nicolas Auguste Tissot in the 1800s in order to characterize local distortions due to map projection. It is the geometry that results from projecting a circle of infinitesimal radius (i.e., a perect cricle) from a curved geometric model, such as a globe, onto a flat, two-dimensional map. Tissot proved that the resulting diagram is an ellipse whose axes indicate the two principal directions along which scale is maximal and minimal at that point on the map.	
	
# A single indicatrix describes the distortion at a single point. Because distortion varies across a map, generally Tissot's indicatrices are placed across a map to illustrate the spatial change in distortion. A common scheme places them at each intersection of displayed meridians and parallels. These schematics are important in the study of map projections, both to illustrate distortion and to provide the basis for the calculations that represent the magnitude of distortion precisely at each point."	
	
# Here, the important concept is captured by the question: what happens to a geometric shape when moving from three dimensions (since the earth is spherical), to two dimensions (since maps are flat)?	
	
# In this lab you will be exploring the coordinate reference systems embedded within spatially explicit R objects, and then evaluating which projected coordinate systems are superior for a set of locations.	
	
	
# #### (1) Exploring Coordinate Reference Systems	
	
# Your first task to understand how a coordinate reference system (CRS) is stored in spatially explicit R objects.	
	
	
require(sp)	
data("northAmerica", package = "fes781") #Access polygonal shapefile included	
# with fes781	
plot(northAmerica, col="red") #Take a look	
# What is this object?	
# Because of their complexity (illustrated here)...	
    # Not run	
    # northAmerica	
    # End not run	
# ...helper functions can be used to summarize spatial object's components	
slotNames(northAmerica) #Spatial objects are composed of 'slots' because they 	
# are S4 objects with more controlled properties. For this assignment we care 	
# about the 'proj4string' slot	
northAmerica@proj4string	
str(northAmerica@proj4string) #What is this?	
	
# When we look at the structure of the proj4string slot, we can see that it is the formal class CRS and that contains a single sub-slot names projargs, within which there is a character string. This character string is a proj4 string that describes the CRS used by the R object.	
	
# You can extract and assign character strings from and to the proj4string slot of a spatial object as you would extract or assign data to any variable. When you first create spatial data from tabular data, there will not be a defined coordinate reference system. This is sometimes true for data that appears to be spatial from the outset. Consider the following example, in which we create a SpatialPolygons object out of latitude and longitude coordinates:	
	
# Create a SpatialPolygons object from latitude and 	
m <- matrix(c(51.456,51.456,51.456,52.567,52.567,52.567,52.567,51.456), ncol=2, 	
            dimnames = list(1:4, c("x", "y")), byrow = TRUE) # Create base matrix	
p <- Polygons(srl=list(sp::Polygon(rbind(m, m[1,]))), ID="1") #Create Polygons	
# object	
sP <- sp::SpatialPolygons(Srl = list(p)) #Create SpatialPolygons object	
class(sP) #Check the class	
plot(sP, col="orange") #Visualize	
sP@proj4string #Access CRS	
	
# Notice that even though we have supplied latitude and longitude values, this dataset doesn't not have a coordinate reference system. Here we are employing relative referencing. To fix this object in geographic space, we need to tell R what CRS was used to _obtain_ the original coordinates. Sometimes we don't have this information, which is problematic. By default, GPS units (a common source of point data) capture coordinates using WGS84. A GPS unit's CRS can be changed, but the uninformed user generally doesn't do this. It is not always the case that latitude and longitude have been provided using WGS84 coordinates, though it is common. Beware!	
	
# Since we know that these coordinates were gathered using WGS84, we can define (or assign) the correct CRS to our R object. This is _not_ the same as projecting the object. Here we are only telling R information it should already know; we are not performing a spatial transformation or warping. Assignment to the proj4string slot requires that you know what the character string is for a given CRS.	
	
# browseURL("http://spatialreference.org/ref/epsg/4326/") #Open the relevant 	
# webpage	
myProj4 <- "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs" #Copy the Proj4 	
# string for WGS84 from the website	
myCRS <- CRS(myProj4) #Use the `CRS()` function to convert the string into a CRS 	
# object	
sP@proj4string <- myCRS #Assign WGS84 into `proj4string` slot	
str(sP) #Check that everything went as planned	
	
# Once a spatial object has a CRS, we can perform a re-reprojection, or spatial transformation (or warping, in some programs), to algorithmically convert one set of coordinates to their equivalent in a new CRS. For vector data, this is accomplished as follows:	
	
# browseURL("http://spatialreference.org/ref/epsg/nad83/") #Identify a new Proj4	
# string you wish to use. Here we employ another GCS named the North American 	
# Datum of 1983.	
newCRS <- CRS("+proj=longlat +ellps=GRS80 +datum=NAD83 +no_defs") #Proj4 string 	
# for NAD83	
(sPNAD83 <- spTransform(x = sP, CRSobj = newCRS)) #Perform 	
	
# Raster datasets that do not have a defined CRS will also need to have one assigned. For spatial transformations of rasters, you need to use projectRaster() instead of spTransform().	
	
require(raster)	
require(fes781)	
rUTM <- raster(system.file("extdata/kroon_hall.tif", package="fes781")) #Load 	
# raster included in fes781	
rUTM@crs #What's the current CRS?	
# Project raster using a CRS for CT State Plane in meters	
rSP <- projectRaster(from = rUTM, crs = "+proj=lcc +lat_1=41.2 	
      +lat_2=41.86666666666667 +lat_0=40.83333333333334 +lon_0=-72.75 	
      +x_0=304800.6096 +y_0=152400.3048 +ellps=GRS80 +units=m +no_defs ") 	
par(mfrow=c(1,2))	
image(rUTM) #Visualize	
image(rSP, add=FALSE, col = gray.colors(255, start = 0.3, end = 0.9, 	
                                        gamma = 1.8, alpha = NULL))
par(mfrow=c(1,1))
	
	
# #### (2) Finding proj4 strings	
# While defining a CRS and projecting to a new CRS are easy enough, they require that we know the Proj4 string associated with the CRS of interest. [spatialreference.org](http://spatialreference.org/) is a useful website that stores a large number of CRS in a number of different formats. You can read about the website [here](http://spatialreference.org/about). The proj4string of the northAmerica data corresponds with the World Geodetic System of 1984. Trying using the spatialreference.org search engine to look up "WGS 1984". The first of many pages contains dozens of coordinate systems. Why?	
	
# Because WGS 1984 (often abbreviated as WGS84) is a geographic coordinate system (GCS), it underlies a large number of projected coordinate systems (PCS). The one we are interested in lies on the third page of the search results, and is tagged with EPSG:4326. Because this gets lost in the hundreds of CRS that rely on WGS84, some copies have been placed on the first page by users: SR-ORG:3, SR-ORG:4, SR-ORG:5, SR-ORG:14. As a side note, EPSG standard for the European Petroleum Survey Group, which maintains CRS standards for many (but not all) CRS.	
	
# If you visit the [EPSG:4326 page](http://spatialreference.org/ref/epsg/4326/) you will see some basic descriptive information and 15 or links. Take a look at a few of the links. You will notice that they are all variations on the proj4 string we found in the proj4string slot. If you look at the "Proj4" link you will find a character string. This is the character string that defines this CRS using syntax compatible with the PROJ.4 library that is bound into key spatial packages in R. In practice, you simply copy this character string from the spatialreference.org website, and use it in R.	
	
	
# #### (3) Exploring Tissot's Indicatrix	
# Note: This remaining portions of the lab draw on an initial implementation of Tissot's Indicatrix from Bill Huber, with extensions by Manuel Gimond and myself.	
	
# Now that you know how to acquire proj4 strings, we can look at how projections cause distortion, and try to identify the best projections for certain locations. First, just take a look at what we are making:	
	
	
globalTissot(p="+proj=moll",  param="+lon_0=-100")	
	
	
# This map uses the Mollweide projection centered at a longitude of 100&deg; West. Each circular form contains two separate ellipses. One shows the shape of a perect circle, and the other shows the ellipse that results from projecting a perfectly circular form from three to two dimensions by way of the projection listed at the top of the map. The latter is the indicatrix. The green and red lines indicate the magnitude and direction of the major and minor ellipse axes, respectively. We can use these lines to evaluate the distortion, since the green line shows the maximum scale distortion and the red line the minimum scale distortion. Importantly, the distortion speaks only to the place where a given indicatix is centered, and not to the spatial extent of the map, which is arbitrary.	
	
# The globalTissot() function takes four possible arguments:	
	
# p = the projection name as defined in the Proj.4 library	
# param = additional parameters to be passed to the projection such as standard parallels and datums (look closely at the Proj4 strings)	
# lon.lim = east-west bounds for output map (note that the maximum extent is limited to the continental US) (optional).	
# lat.lim = north-south bounds for output map (note that the maximum extent is limited to the continental US) (optional).	
# ex = List pre-defined projection parameters using projection defined by parameter p (optional).	
	
# For greater clarity, we can focus on a reduced region by limiting the lon.lim and lat.lim arguments of globalTissot(), or we can use localTissot().	
	
proj.in  <- CRS("+proj=longlat +datum=WGS84") #Reference system used to record 	
# lat/long values. !!NOTE: This is held constant for this entire document!!	
proj.out <- CRS("+proj=moll +lon_0=-100")     #Reference system we are projecting	
# to	
lat  <- 41.316838 #The latitude of Kroon Hal room G01	
long <- -72.923657 #The longitude of Kroon Hal room G01	
ti <- localTissot(long, lat, proj.in,proj.out)	
	
	
# As with globalTissot(), we have a perfect circle (buff color), and a distorted circle (blue). The green and red lines show magnitude and direction of the major and minor axes, respectively, and the the dotted lines show true North-South and East-West.	
	
# The Mollweide projection is an "equal-area" projection. While distortion may occur with respect to shape or angular relationships, the area is well preserved. We can see this and other properties of the indicatrix by looking at the object into which we placed the localTissot() results.	
	
str(ti) #Look at the objects structure	
ti$max.scale	#Maximum scale of distortion	
ti$min.scale	#Minimum scale of distortion	
ti$scale.meridian	#Meridian scale factor (i.e., how much longer or shorter is 	
# the green line relative to the dashed meridian?)	
ti$scale.parallel	#Parallel scale factor (i.e., how much longer or shorter is 	
# the red line relative to the dotted parallel?)	
ti$scale.area	#Area scale factor	
ti[[12]][1]	#Maximum angular deformation. At any location, there is one set of 	
# lines that will have a more severe angular deformation than any other pair. 	
# This deformation is measured in degrees.	
ti[[12]][2]	#Angle between true north and grid north	
ti[[12]][3]	#Angle between parallel and meridian	
	
# While Tissot's indicatrix can't help us understand the distortion of direction, from these other measures we can get a good sense for whether or not a projection is performing as we hope. Here we find that areal distortion is essentially 0, as we would hope from an equal-area projection. The projection appears to overstate the size of forms across both meridians and parallels by tiny amounts. For this projection, we might preserve area but this comes with a concommitant and appreciable angular distortion.	
	
# Are there thresholds for what constitutes acceptable, when evaluating projection distortion? No. Usually this is determined by the map producer, though one could argue that the best projection is that which contains the least distortion for the properties of interest. For example, if you were working on a project analyzing the areas of land masses, you wouldn't want to use a conformal projection.	
	
	
# #### (4) A Few Examples	
# A conformal (angle preserving) projection	
	
globalTissot(p="+proj=tmerc", param="+lat_0=0 +lon_0=-100")	
proj.out <- CRS("+proj=tmerc +lat_0=0 +lon_0=-100")	
TI <- localTissot(long, lat, proj.in, proj.out)	
	
	
# Seems pretty good at preserving angles between meridians and parallels, however area and direction are off. To improve, let's re-center the projection along the meridian that runs through New Haven.	
	
globalTissot(p="+proj=tmerc", param="+lat_0=0 +lon_0=-72.923657")	
proj.out <- CRS("+proj=tmerc +lat_0=0 +lon_0=-72.923657")	
TI <- localTissot(long, lat, proj.in, proj.out)	
	
	
# Notice that the red and green lines are similar lengths and the we can't see any circular distortion.	
	
# An equidistant, conic projection (distance preserving)	
	
globalTissot(p="+proj=eqdc", param="+lat_1=30 +lat_2=45 +lon_0=-100")	
proj.out <- CRS("+proj=eqdc +lat_1=30 +lat_2=45 +lon_0=-72.9279")	
ti <- localTissot(long, lat, proj.in, proj.out)	
ti$max.scale	#Maximum scale of distortion	
ti$min.scale	#Minimum scale of distortion	
ti$scale.meridian	#Meridian scale factor	
ti$scale.parallel	#Parallel scale factor	
ti$scale.area	#Area scale factor	
ti[[12]][1]	#Maximum angular deformation	
ti[[12]][2]	#Angle between true north and grid north	
ti[[12]][3]	#Angle between parallel and meridian	
	
	
# Seems pretty great!	
	
# A Universal Transverse Mercator (UTM) projection	
	
globalTissot(p="+proj=utm", param="+zone=19") #Zone 19-north contains New Haven	
	
	
# We can modify the UTM projection by defining a custom central meridian (+lon_0=-72.923657) and a different datum (+datum=NAD83) as shown in the following block of code.	
	
globalTissot(p="+proj=utm", param="+lon_0=-72.923657 +datum=NAD83")	
	
	
# Clearly UTM is poor for large longitudinal swaths, but how about for our specific area of interest? Here we use the lon.lim argument since each UTM zone spans 6 degrees of longitude.	
	
globalTissot(p="+proj=utm", param="+lon_0=-72.923657 +datum=NAD83", 	
             lon.lim = c(long-3, long+3), lat.lim  = c(lat-10, lat+10))	
proj.out <- CRS("+proj=utm +zone=19 +lon_0=-72.923657 +datum=NAD83")	
ti <- localTissot(long, lat, proj.in, proj.out)	
	
	
# This CRS does an excellent job. If you inspect the object properties you will see the area, maximum, minimum, and angular scales of distortion are all neglible. The red and green axes don't appear because the limited distortion.	
	
	
# #### (5) Assignment	
# Select a location of interest in the U.S. Obtain the latitude and longitude for that location and use the globalTissot() and localTissot functions to evaluate the following coordinate systems. remember to store the localTissot() results into an object for inspection. The CRS names listed here will be good search terms to use in [spatialreference.org](http://spatialreference.org/).	
	
# USA Contiguous Lambert Azmuthal Equal Area (USGS-EDC)	
# USA Contiguous Albers Equal Area Conic	
# Q: Is one better than the other, and why?	
	
	
# World Azimuthal Equidistant	
# Q: Is this projection suitable as it stands? If not, could we modify the CRS parameters to decrease the distortion?	
	
	
# WGS 84 / World Mercator	
# North America Lambert Conformal Conic	
# Q: Which of these is better for navigation?	
	
	
# The correct State Plane CRS for your location	
# The correct UTM for your location	
# Q: Which one returns more accurate areal estimates, and why do you think this is?	
	
	
