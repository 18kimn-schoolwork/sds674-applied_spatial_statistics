---
title: "Homework 2"
author: "Nathan Kim"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE} 

# include=FALSE suppresses the code and output from this "chunk" look at https://yihui.org/knitr/options/#option-templates for other options 

knitr::opts_chunk$set(echo = TRUE)

# sets global options to echo = TRUE, suppresses code output, see https://yihui.org/knitr/options/#option-templates. 

# load in necessary packages and files here, the code and output will be suppressed by include = FALSE
library(fes781)
library(tidyverse)
library(sp)
```

### Tissot's Indicatrix

Obtain the latitude and longitude of Yale Meyers Forests in Union, CT. 

*According to Google Maps, Yale-Myers Forest is located at 41.909814, -72.155114.*

### 1. 

Between “USA Contiguous Lambert Azmuthal Equal Area” [SR-ORG:6903] and “USA Contiguous Albers Equal Area Conic” [ESRI:102003] Is one better than the other, and why?

```{r}
#small note: the tidyverse syntax is used here to create tibbles

lambert <- CRS("+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 
               +a=6370997 +b=6370997 +units=m +no_defs")
albers <- CRS("+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=37.5 +lon_0=-96
              +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs")
wgs84 <- CRS("+proj=longlat +datum=WGS84")
lat <- 41.909814
long <- -72.155114

#Small function to create a tibble with the values provided by localTissot()
make_tissot_tibble <- function(proj.out, shortname, long = -72.155114, lat = 41.909814, 
                               proj.in = CRS("+proj=longlat +datum=WGS84")){
  tissot <- localTissot(long, lat, proj.in, proj.out)
  tissot <- c(tissot[3:11], unlist(tissot[12])) %>%  
  as_tibble %>% 
  pivot_longer(cols =everything(), names_to = "Measures",
               values_to = paste(shortname, "Projection Values")) %>% 
  mutate_at(2, list(~round(.,4)))
  return(tissot)
}
lamberttissot <- make_tissot_tibble(lambert, "Lambert")
alberstissot <- make_tissot_tibble(albers, "Albers")

full_join(lamberttissot, alberstissot, by = c("Measures")) 
```

From the Yale Meyers Forest, the USA Contiguous Albers Equal Area Conic appears to be marginally better because it produces a slightly smaller max scale of distortion and a slightly larger min scale of distortion, as well as a smaller angle of deformation and a smaller convergence angle, than the Lambert projection. Its meridian scale factor and parallel scale factor are also closer to 1 compared to the Lambert projection, indicating that distance-preservation is very slightly better. 


### 2. 

“World Azimuthal Equidistant” [ESRI:54032]. Is this projection suitable as it stands? If not, could we modify the CRS parameters to decrease the distortion?

```{r}
azimuthal <- CRS("+proj=aeqd +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs")
aziTissot <- make_tissot_tibble(azimuthal, "Azimuthal Equidistant")

aziTissot
```

It doesn't appear to be suitable, as the major axis of the ellipse is much larger than that of a perfect circle. This is also true numerically at a scale factor of 1.378. The direction of the projection is also off. 

We can modify the CRS by centering it at the meridian running through Yale-Meyers Forest: 

```{r}
azimuthal_modified <- CRS(paste0("+proj=aeqd +lat_0=0", " +lon_0=", long," +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs"))

aziTissot_modified <- make_tissot_tibble(azimuthal_modified, "Modified Azimuthal Equidistant")
full_join(aziTissot, aziTissot_modified)


```


Although not perfect, this greatly reduces the distortion and moves the meridian scale, parallel scale, scale of area, and max scale closer to 1. The angle of deformation is also lowered greatly to 5.173 degrees. 

### 3. 

Between “WGS 84 / World Mercator” [EPSG:3395] and “North America Lambert Conformal Conic” [ESRI:102009]. Which of these is better for navigation?


```{r}

wgs84tissot <- make_tissot_tibble(CRS("+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 
                                      +datum=WGS84 +units=m +no_defs"), "WGS84")
lambertconformal <- make_tissot_tibble(CRS("+proj=lcc +lat_1=20 +lat_2=60 +lat_0=40 
                                           +lon_0=-96 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs"), 
                                       "Lambert Conformal")

full_join(wgs84tissot, lambertconformal, by = "Measures")
```

Neither of these are perfect to begin with; the WGS 84 / World Mercator has preserved direction but area on a distorted scale with a meridian scale factor of .939 and a scale parallel factor of 1.34. On the other hand, the second has only a minor distortion of area but angle is not preserved -- it has a converge of 15.667 degrees, implying that the direction of north is off 15.667 degrees from the true direction of north. 

Although the World Mercator projection has inaccurate distances, it preserves direction and thus ensures that a plotted course can be traveled correctly. According to Wikipedia, the definition of the Mercator projection also allows plotted courses to be modeled as straight lines instead of curved ones. 

### 4. 

Between the correct State Plane CRS for Yale Meyers, using the North American Datum of 1983 and a linear unit of meters and the correct UTM for Yale Meyers, using the North American Datum of 1983 and a linear unit of meters. Which one returns more accurate areal estimates, and why do you think this is?


```{r}
stateplane <- make_tissot_tibble(CRS("+proj=lcc +lat_1=41.2 +lat_2=41.86666666666667 
                                     +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096 
                                     +y_0=152400.3048 +ellps=GRS80 +units=m +no_defs"),
                                 "State Plane")

utm <- make_tissot_tibble(CRS("+proj=utm +zone=19 +datum=NAD83"), 
                          "Correct CT UTM")

full_join(stateplane, utm, by = c("Measures"))



```

Although they are incredibly similar, we see that the state plan projection has a very slightly better scale area than the UTM for CT. Intuitively, this is because the state plane projection is more specific than the UTM projection, since one is specific to state (and often there are multiple per state) and UTM is specific to 6 degrees of longitude, which is small but still usually more general than a state. In our case this holds up; the state plane projection is based on Connecticut, which is definitely smaller than a single UTM zone. Thus the areal estimates would be more accurate for the state plane projection. 