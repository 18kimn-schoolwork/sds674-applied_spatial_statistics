---
title: "Property, Race, and New Haven"
subtitle: "A Spatial Analysis of New Haven Property Values"
author: "Nathan Kim, John Park"
date: "May 3, 2020"
output:
  pdf_document:
    keep_tex: yes
  html_document:
    df_print: paged
  word_document: default
header-includes: \usepackage{dcolumn}
fig_caption: yes
urlcolor: blue
---

```{r setup, include=FALSE}
#since they said to suppress all R code and output -- 
knitr::opts_chunk$set(echo = F, warning = F, message = F)
library(sf)
library(sp)
library(spdep)
library(stargazer)
library(spatialreg)
#library(rspatial)
library(spgwr)
library(viridis)
library(cwi)
library(ggthemes)
library(tidyverse)
```

```{r import}
proj.out <- CRS("+proj=lcc +lat_1=41.2 +lat_2=41.86666666666667 +lat_0=40.83333333333334 +lon_0=-72.75 +x_0=304800.6096 +y_0=152400.3048 +ellps=GRS80 +units=m +no_defs")
#contextual/demographic blockgroup-level variables
census <- read_csv("data/contextual variables.csv")
#shapefile of block groups
nhv_blockgroups <- read_sf(dsn="data/nhv_blockgroups_shapefile.shp") %>% 
  st_transform(crs = proj.out)
#combine addresses and their coordinates
properties <- read_csv("property_info.csv") 
adds <- read_csv("data/nhv_addresses.csv") %>% 
  dplyr::select(-`Unique ID`, -City, -State, -ZIP)
rawproperties <- read_sf(dsn="arcgis_output/nhv_addresses_GeocodeAddress.shp") %>% 
  bind_cols(adds) %>% 
  st_transform(crs=proj.out) %>% 
  bind_cols(properties)

# delete outliers --> 23558 rows
rawproperties <- rawproperties %>% 
  st_join(st_transform(new_haven_sf, crs=proj.out)) %>% 
  drop_na(name) %>% filter(between(improvement_value/living_area, 17, 240)) %>% 
  st_as_sf()

# remove duplicates --> 20387 rows
rawproperties <- rawproperties %>% dplyr::filter(!duplicated(geometry))

#map the new haven point addresses onto new haven block groups
#then aggregate to the tract level 
#merge on tract-level attributes
properties <- rawproperties %>% 
  mutate(value = (improvement_value + land_value)) %>% 
  dplyr::select(value, year_built, area = gross_area)

dt_polygons <- nhv_blockgroups %>% 
  st_join(properties) %>% 
  drop_na(value) %>% 
  group_by(GEOID) %>% 
  summarise(value = median(value, na.rm=T), 
            area = median(area, na.rm=T),
            year_built = median(year_built, na.rm=T)) %>% 
  left_join(census, by = "GEOID") %>% 
  left_join(nhv_blockgroups, by = "GEOID") %>% 
  st_as_sf()
  
#map the new haven tract information onto each house
# JP: i'm kind of confused about what we're trying to do here
properties_with_cvs <- nhv_blockgroups %>% 
  st_join(properties) %>% 
  drop_na(value) %>% 
  left_join(census, by = "GEOID")
  
```



# Introduction

Segregation and access to property ownership have been a large source of controversy, debate, and policy over the past hundred years. One can refer to the Boston Fed's 1992 study on mortgage lending that reported that Black applicants were 56% more likely to be denied of a mortgage, or W.E.B. Du Bois' landmark monograph titled *The Philadelphia Negro* that documented segregated spaces reproducing oppression. These and other works have inserted themselves into the canon of many academic fields, from history to political science to even statistics and data analysis.

The topic has been at the center of New Haven activism as well--recently, activists have been specifically criticizing the \$146 Million that Yale withheld in taxes from New Haven in 2018 through an antiquated tax-exemption statement from 1818.^[They’ve Got Yale’s Number | New Haven Independent. (2019, November 14). http://www.newhavenindependent.org/index.php/archives/entry/new_haven_rising/] Given that Yale owns over \$2.5 billion in tax-exempt property, which alone is more than three times more than any other New Haven institution's total property value, as well as the fact that New Haven's total budget is only around $500 Million, many residents have mobilized against Yale's dominance in the New Haven property market.^[Yale’s tax exempt New Haven property worth \$2.5 billion—New Haven Register. (n.d.). Retrieved May 3, 2020, from https://www.nhregister.com/colleges/article/Yale-s-tax-exempt-New-Haven-property-worth-2-5-11372847.php] State legislation has been drafted to change this nontaxation status, but ultimately failed on the grounds that Yale's nearly 200-year-old charter could not be altered without mutual agreement.^[What Yale Could Have Paid – The New Journal. (n.d.). Retrieved May 3, 2020, from http://www.thenewjournalatyale.com/2019/05/what-yale-could-have-paid/]

This situation is made even murkier and controversial given New Haven's rocky history. The city flourished in early years with industrial activity related to Eli Whitney and other inventors, and later due to factories including the Winchester Arms Company. This status changed following World War II, when cities across America collectively experienced "white flight" to the suburbs amidst an influx of African American and Latino residents. Economic activity in this period related to manufacturing moved to the industrial parks in the suburbs.^[New Haven, Connecticut—RWJF. (n.d.). Retrieved May 3, 2020, from https://www.rwjf.org/en/cultureofhealth/what-were-learning/sentinel-communities/new-haven-connecticut.html] Richard Levin took office as Yale President in 1993, and Yale began buying property in the Chapel Street Historic District, taking over what is now known as "the Shops on Broadway.^[Yale’s Property Line Expands Again, to Neighbors’ Ire. (1999, April 26). The New York Times. https://www.nytimes.com/1999/04/26/nyregion/yale-s-property-line-expands-again-to-neighbors-ire.html]

Yale is not unique in its tax-exempt status and this contentious history. Many universities across the country have faced conflicts for their expansions into surrounding cities, sometimes resulting in violent protests like at Columbia University in 1968. However, Yale is distinct in the combination of its massive endowment and property value, the relatively small size of New Haven and the lack of other economic giants, and the unequal racial history that Yale's dominance builds upon. For the purposes of this class, this dynmamic makes New Haven property an especially interesting one for us to study.

We focus in this analysis on different relationships to property values and area for Black and White communities, using administrative data from the City of New Haven as well as from the U.S. Census Bureau. We are interested not in quantifying strict disparities in property values between Black and White residents, but rather examining how race affects relationships between property value and local demographic predictors, as well as how these relationships vary across space. 

# Main Data Sources

We use two sources of data for this project: address-level data from the New Haven Online Assessment Database, and blockgroup-level data from the U.S. Census Bureau. The [Online Assessment Database](http://gis.vgsi.com/newhavenct) contains information on every address in the City of New Haven, including assessments and appraisals yearly since 2016, a breakdown of these values by land value and improvement value, land area and building area, information on "extra features" that usually included elevators, "style" of the building, owners of the building since around 1970, and building codes, among other information. Because many variables were categorical variables with many values, not lending easily to an ANOVA-type analysis, we use only property value, property area, and geographic location in our analysis. We obtained these data by scraping through every page on the database, in total collecting information on 27,395 properties. Because this resource provided addresses but not longitude-latitude coordinates that were needed for spatial analysis, we used the ESRI World Geocoding Service provided in ArcGIS Pro with credits provided by Yale University. 

Our second source of data comes from the 2010 Census and the 2014-2018 American Community Survey (ACS), both created and compiled by the U.S. Census Bureau. While the Census is able to survey nearly everyone in the United States, it becomes prohibitively expensive to collect the same amount of information on many variables of interest, including educational attainment, occupation, means of transportation to work, and so on. Moreover, it is impossible to obtain information at this magnitude every year and thus provide detailed trends data for every state and county. The ACS was created in 2003 to counter this issue by surveying a different sample of the population every year on many demographic variables. It is thus smaller than the Census, but still the largest survey in the U.S. with about two million respondents every year. We take data from the 2014-2018 5-year ACS, a source that compiles data over five years to provide reliable, non-identifiable information at granular geographic levels. 

Like many projects using demographic data, the quality of our data was a recurring obstacle. The ACS, like many surveys, limits the availability of data at small levels to protect the privacy of its respondents and because the margin of the error at small levels is too large to release data. The survey agggregates data over five years to combat both of these issues, but still many variables are suppressed at the level of our interest -- for example, median household income and educational attainment. The New Haven Online Assessment Database is similarly limited in that it provides a wealth of information, but there is no indicator on the race of the property owner. This limitation is understandable given that "race of property owner" is often not well-defined or meaningful, for instance in the case of large institutions like the City of New Haven or Yale University, as well as in the case of renters who do not own the property they live in but who we want to account for in our analysis.

To strike a balance between the number of variables we wanted to study, the granularity of our data and the "ecological fallacy," the number of observations to obtain reasonable degrees of freedom, and the margin of error for each variable, we pulled "block group"-level estimates from the ACS and 2010 Census and matched them with property data through a spatial join. That is, properties were assigned block group identifiers depending on where they resided, and all of our data were aggregated to block group-level estimates. 

From the ACS and Census Bureau, we pull: 

- **Proportion of respondents in a white-collar occupation**, which we defined as management, business, science, arts, sales, and office occupations. 
- **Proportion of respondents who are undergraduate or graduate students.**
- **Proportion of respondents without broadband internet**, which includes those without a computer in their household, those with a computer but no internet, and those with a computer and only dial-up internet. 
- **The total number and the proportion of respondents of each race.**
- **The proportion of respondents who are renting their current residence.** 
- **Average number of people residing in the household or address.**


# Shapefiles and projections 

Our blockgroup-level shapefile was obtained from the Census Bureau's TIGER/Line program through the `tigris` package in R. We also used a shapefile from DataHaven, a New Haven nonprofit, to subset this shapefile and obtain shapes that were strictly limited to the City of New Haven as opposed to New Haven County. 

All analysis pertains to New Haven, CT. This allows us to use the highly accurate State Place Coordinate System for Connecticut. This is a Lambert Conformal Conic projection which preserves shapes, angles and directions. At such a small area, distance and areal accuracy is preserved as well.

# Descriptive Statistics and Summary Information

To introduce our data, we present: 

1. A table of summary statistics for each variable, divided by race. 
2. A map of New Haven property values by address, overlaid with New Haven blockgroups. 
3. A matrix plot of our continuous variables. 

For the map of property values, several blank swatches are present; either no properties are here or they belong to large facilities (e.g., forests, or Union Station and the nearby yard). As common sense suggests, we can see that property values are higher near Downtown New Haven and East Rock. 

```{r, results = "asis"}
dt_polygons %>% 
  select(-GEOID) %>% st_drop_geometry() %>% as.data.frame() %>% 
  mutate(hhsize = hhsize/100) %>% 
  rename_all( ~c("Median Property Value", 
                                 "Median Area of Property", 
                                 "Median Year Built", "Race", 
                                 "Proportion in White-Collar Occupations", 
                                 "Proportion University Students", 
                                 "Proportion without Broadband Internet", 
                                 "Average Population in Blockgroup", 
                                 "Proportion of Total Population", 
                                 "Proportion Renting Current Residence", 
                                 "Average Household Size")) %>% 
  pivot_longer(-Race) %>% group_by(Race, name) %>% 
  summarize(N = sum(!is.na(value)), 
            Mean = mean(value, na.rm=T), 
            St.Dev = sd(value, na.rm=T)) %>% 
  ungroup() %>% 
  mutate_if(is.numeric, ~round(.,2)) %>% 
  mutate(Race = ifelse(name == "Average Household Size",Race, ""),
         Race = case_when(Race == "B"~ "Black", Race == "W" ~ "White", T ~ "")) %>% 
  rename(" " = name) %>% 
  stargazer(summary = F, header = F, title = "Descriptive Statistics")
```

```{r, warning=  F, message = F}
#accessories to build the descriptive map
#this selects deletes any properties with weird geocoordinates that throw off the map below
#also removes outliers so that the color scale makes sense 
#subsets the nhv_blockgroups shpfile to only include those that are strictly new haven according to the datahaven sf object
nhv_geoids <- st_centroid(nhv_blockgroups) %>%
  st_join(st_transform(new_haven_sf, st_crs(nhv_blockgroups))) %>% 
  drop_na(name) %>% pull(GEOID)
nhv_city <- nhv_blockgroups %>% filter(GEOID %in% nhv_geoids) %>% 
  st_as_sf() 
```

```{r,  fig.width = 5, fig.height = 4, dpi = 600, fig.cap = "\\label{fig:simple_map} Map of New Haven Property Values, in Dollars per Square Foot"} 
ggplot() + 
  geom_sf(data = rawproperties, 
          aes(color = improvement_value/living_area), 
          size = .8, alpha = .5) + 
  geom_sf(data = nhv_city, fill = NA, size = .7, color = "firebrick3") + 
  scale_color_viridis() + 
  labs(color = "Land Value, in Dollars/SqFt") + 
  theme_map()

```


We display a matrix plot comparing the proportion non-white, the logged median property value, the logged median property area in a blockgroup, and the proportion renting in a given area. We can see that even with the log transformation, both the property value and property area are right-skewed and do not appear normally distributed. 

```{r matrix_plot, fig.width = 7, fig.height = 7, fig.cap = "\\label{fig:matrixplot} Matrix Plot of Continuous Variables"}
source("http://www.reuningscherer.net/s&ds230/Rfuncs/regJDRS.txt")
#lol stealing one of JDRS' functions
matrix_dt <- dt_polygons %>% 
  filter(race == "B") %>% 
  mutate(value = log(value), area = log(area)) %>% 
  select(Prop = prop, LogValue = value, LogArea = area, renting, in_uni,
         white_collar, NoInternet = no_broadband, Size = hhsize) %>% 
  st_drop_geometry() 

matrix_dt %>% 
  pairsJDRS(cex.labels = .01)
text(seq(.07, .81, length.out = 8), .98,
     names(matrix_dt),
     xpd=TRUE, adj=c(0,.5), cex=.8)


```


# Analysis


## Spatial association

To give a glimpse of the spatial association of property values and justify using a spatial lag model, we performed global Moran's I, Geary's C, and local Moran's I analyses on logged property values at the blockgroup level. We used a rook adjacency measure for connectivity, a diagram of which can be found below. We also show a scatterplot of local Moran's I values with the global Moran's I and Geary's C results as captions. 

```{r}
dt_polygons_1 <- dt_polygons %>% 
  select(geometry, GEOID, everything())
# create SpatialPolygonsDataFrame
SPDF <- dt_polygons_1 %>%  as("Spatial")
SPDF$logvalue <- log(SPDF$value)


coords <- coordinates(SPDF) # obtain the centroids of the features
ids <- row.names(as(SPDF, "data.frame")) # make an index vector
rookNB <- spdep::poly2nb(SPDF, queen = FALSE) # rook adjacency

rookNBW <- nb2listw(rookNB, glist=NULL, style="W", zero.policy=TRUE)
```


```{r rooknb_plot,  fig.width = 4, fig.height = 4, fig.cap = "\\label{fig:rooknb} Diagram of New Haven Rook Adjacency Connections"}
#plot(rookNB, coords, pch=20, cex=0.5, col="gray"); title("Rook Adjacency")
# creating row standardized spatial weights from neighbor list

rooksf <- as(nb2lines(rookNB, coords = coords), "sf") %>% 
  st_set_crs(proj.out)


ggplot(dt_polygons) + 
  geom_sf(fill = 'lightcyan', aes(color = "Blockgroup Boundaries")) +
  geom_sf(data = rooksf, size = .7, aes(color = "Neighbors Connections")) + 
  scale_color_manual(values = c("Blockgroup Boundaries" = "black", 
                                "Neighbors Connections" = "red")) + 
  theme_map() + theme(text = element_text(size = 14)) + 
  labs(color = "Legend")
```

```{r globalmoran_gearysc}
moransi <- moran.test(x = SPDF$logvalue, listw=rookNBW, zero.policy=T)
morancaption <- paste0("Global Moran's I = ", round(moransi$estimate[1],3), 
                  ", Expectation = ", round(moransi$estimate[2], 3),
                  ", p = ", 
                  formatC(moransi$p.value, digits = 3, format  ="e" ))

gearysc <- geary.test(x = SPDF$logvalue, listw=rookNBW, zero.policy=T)
gearycaption <- paste0("Geary's C = ", round(gearysc$estimate[1],3), 
                  ", Expectation = ", round(gearysc$estimate[2], 3),
                  ", p = ", 
                  formatC(gearysc$p.value, digits = 3, format  ="e" ))


```



```{r localmoran_setup}

localM <- localmoran(SPDF$logvalue, listw=rookNBW, zero.policy=TRUE)
SPDF$localMoranZ <- abs(localM[,4]) ## Extract z-scores
lmPalette <- colorRampPalette(c("white", "red"), space="rgb") # colors

miPalette <- colorRampPalette(c("firebrick1", "gray31"), space = "rgb") #A palette
nInts <- 50 #How many colors/intervals do you want?
myPal <-miPalette(n=nInts) #Store your color palette
classesQuantile <- suppressWarnings(classInt::classIntervals(localM[which(abs(localM[,5])<=0.1),5],
                                                             n=nInts, style = "quantile")) #Create intervals for your variable (distances)
myCols <- NA #Initialize a vector
# Obtain colors for significant values
myCols[which(abs(localM[,5])<=0.1)] <- classInt::findColours(classesQuantile, myPal)
myCols[which(abs(localM[,5]) > 0.1)] <- "gray31" #Assign non-significant values a gray color
```

```{r localmoran_plot,  fig.width = 7, fig.height = 4, fig.cap = "\\label{fig:localmoran} Diagram of Local Moran's I for New Haven Property Values"}
par(mar = c(5.1, 4.1, 4.8, 1.8))
moran.plot(SPDF$logvalue, listw=rookNBW, zero.policy=TRUE, xlab="Log Property Value",
           ylab="Log Property Value (Spatial Lag)", col=myCols, pch=20)
title("Local Moran Scatterplot", line = 2.5)
mtext(paste0(morancaption, "\n", gearycaption))
```

Given that our Global Moran's I is much greater than zero, with a p-value <<< .005, the test indicates positive spatial autocorrelation of property values. Higher property values are likely to be clustered together, and lower property values are likely to be clustered together. In agreement with Moran's I, Geary's C tells us that there is statistically significant positive spatial autocorrelation, given that 0 conveys perfect positive spatial autocorrelation. In both cases the coefficients edge closer to 1 and 0 respectively than their expectations under the null hypothesis, lending strong evidence to show that a regression on these data should account for spatial variation. 

This is mostly supported by the local Moran plot of logged property values and their corresponding spatial lags. Most points are in the upper right and lower left quadrants, meaning that there is a positive association between most locations and their spatially lagged counterparts. We see a large degree of clustering (consistent with the global statistic). Although the global Moran's I captures most of the local trends, there are multiple exceptions. In the bottom right quadrant, we see a few high value blocks with neighboring low value blocks. In the top left quadrant, we see several low value blocks with neighboring  high value blocks. However, these local I values are mostly not statistically significant. The signficant I values are found in the top right and bottom left quadrant, further supporting the global statistic.

# Spatial error model

We present a spatial error model at the block group level here, again using rook adjacency. To reiterate, we are interested in the relationships between several demographic predictors and property values for Black and White residents. Though we cannot obtain information that can classify properties as either Black-owned or White-owned, we try to work around this by incorporating demographic information that is race-specific. That is, "renting" is defined separately for proportion of Black residents who are renters and White owners who are renters, and so on. 

It may have been optimal to use a spatial lag model instead of a spatial error model to account for variations not just in the residuals but in the predictors and outcomes themselves. We use a spatial error model because survey weights are not implemented in R for spatial lag models, but we consider it an important part of our regression to account for heterogenous population distribution across space. The error model also has benefits in that the coefficients do not have to be separated into "direct" and "indirect" effects for interpretation, as spatial lag models require. 

We use three response variables here: logged property value, logged property area, and a "dollars per square foot" measure that is simply value divided by area. These are labeled in the regression table, produced through the `stargazer` package in R. 

```{r polygons, results = "asis"}

polygon_mods <- map(c("B","W"), function(race){
  dt <- dt_polygons %>% filter(race == !!race, GEOID %in% nhv_geoids) %>% 
    select(-GEOID, -race) %>% 
    drop_na() %>% st_as_sf() %>% 
    filter(between(value/area, .15, 127))
  polys <- dt %>% 
    poly2nb(queen=F) %>% 
    nb2listw(zero.policy = T)
  dt <- st_drop_geometry(dt)
  values_mod <- errorsarlm(log(value)~ . - tot - area , dt, polys, 
                           weights = tot, 
                           na.action =na.exclude, zero.policy = T)
  areas_mod <- errorsarlm(log(area) ~ . - tot - value, dt, polys, 
                          weights = tot, 
                          na.action =na.exclude, zero.policy = T)
  both <- errorsarlm(value/area ~ . - tot - value -area, dt, polys, 
                     weights = tot, 
                     na.action =na.exclude, zero.policy = T)
  return(list(values_mod, areas_mod, both))
}) %>% 
  reduce(c) #lol neat trick with reduce() + c() to unnest one level nicely 
polygon_mods <- polygon_mods[c(1,4,2,5,3,6)]
lambdas <- unlist(map(polygon_mods, function(x) summary(x)$lambda)) %>% 
  round(digits = 3)
pvalue <- unlist(map(polygon_mods, function(x) summary(x)$LR1$p.value)) %>% 
  formatC(format = "e", digits = 2)

stargazer(polygon_mods,header = F,
          title = "Spatial Error Model Results", 
          column.labels = rep(c("Black", "White"), 3), 
          column.separate = rep(1,6), 
          add.lines = list(c("Lambda (Spatial dependence)", lambdas),
                           c("Spatial dependence p-value", pvalue)), 
          omit.stat = c("aic", "ll", "lr", "wald"))
#sigh after all of this i'm not sure about hwo to approach this regression ... can't really compare between groups... don't know
#whatever just an s&ds final proj
```

We present the spatial error model in Table 2. There are both intuitive and unexpected results. Although significance varies, we generally find that higher proportions of Black residents decreases property values and area in a block group while the opposite is true for the proportion of White residents. Interestingly, the proportion of renters in an area increases property values and area for both White and Black residents, and in the case of price per square foot, the effect appears nearly identical across Black and White races. Household size appears to show some differences between Black residents and White residents, with larger Black households being associated with lower value, area, and value/area and the opposite appears tentatively for White residents. However, we do not find significance between the two sets of coefficients. 

Notably, in all models, the lambda value indicating spatial dependence in the error model is numerically high and significantly different from zero. This is consistent with our initial analyses with Moran's I and Geary's C, indicating that models with property value, property area, and value/area benefit from considering spillover effects from neighboring block groups. 


## GWR 

To examine how these relationships vary across space (as opposed to just accounting for spatial variation as we do above), our next analysis performs a geographically weighted regression at the block group level. To simplify interpretation and present only a few maps, we included the proportion that each race occupied in a block group and the race-specific proportion of renters in an area. 

We used `gwr.sel()` to obtain bandwidths with a Gaussian gweight as demonstrated in class. As Professor Gregoire mentioned, the output is verbose and not readily interpretable; we present the global coefficients as a snapshot of the model results (Table 3), and present two maps of the coefficients for "proportion of race that is renting" and "this race's proportion of total population" in order to visualize our results. 

Expectedly, we find a strong amount of geographic variation in both coefficients. For Black residents, more renters in The Hill was associated with increased property value compared to other neighborhoods. For White residents, more renters in Downtown New Haven and Wooster Square was associated with property value. For Black residents, composing a higher proportion of the population in the Long Wharf and Fair Haven neighborhoods seems to decrease property value more than other areas. For White residents, composing a higher proportion of the population was associated with more increases in property value in the East Rock and Prospect Hill neighborhoods. 


```{r}
run_gwr <- function(race){
  dt <- dt_polygons %>% filter(race == !!race, GEOID %in% nhv_geoids) %>% 
    select(-GEOID, -race) %>% 
    drop_na() %>% st_as_sf() %>% 
    filter(between(value/area, .15, 127)) %>% 
    as("Spatial")
  
  bw <- gwr.sel(log(value) ~ renting + prop, data = dt, verbose = F, 
                gweight = gwr.Gauss)
  mod <- gwr(log(value) ~ renting + prop, data = dt, 
             bandwidth =  round(bw, 0), 
             gweight = gwr.Gauss, hatmatrix = T)
  
  modlm <- mod$lm
  rsq <- sum(modlm$residuals^2)/sum((log(dt$value) - mean(log(dt$value), na.rm=T))^2)
  result <- list(mod, rsq = rsq, obs = nrow(dt))
  return(result)
}

gwr_result <- map(c("B","W"), run_gwr)

raw_results <- map(c("B","W"), function(race){
  res <- gwr_result[[which(c("B","W") == race)]]
  title <- ifelse(race == "B", "Black Residents", "White Residents")
  base <- res[[1]]$lm$coefficients %>% as.data.frame() %>% 
    rownames_to_column("Variable") %>% 
    rename(!!title := ".")
  
  extra <- data.frame(Variable = c("R-Squared","Observations"), 
                      title = c(res$rsq, res$obs)) %>% 
    rename(!!title := "title") 
  
  bind_rows(base, extra) %>% 
    mutate_if(is.numeric, ~round(.,3))
}) %>% reduce(full_join, by = "Variable")

```

```{r,eval = F, results = "asis"}

stargazer(raw_results, summary = F, header= F, title= "Global (OLS) Results from GWR")
```


\begin{table}[!htbp] \centering 
  \caption{Global Results} 
  \label{} 
\begin{tabular}{@{\extracolsep{5pt}} cccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
 & Variable & Black Residents & White Residents \\ 
\hline \\[-1.8ex] 
1 & (Intercept) & $11.643$ & $10.641$ \\ 
2 & renting & $0.007$ & $0.011$ \\ 
3 & prop & -$0.009$ & $0.017$ \\ 
4 & R-Squared & $0.727$ & $0.403$ \\ 
5 & Observations & $101$ & $92$ \\ 
\hline \\[-1.8ex] 
\end{tabular} 
\end{table} 







```{r gwr_plot,fig.width = 6, fig.height = 8, fig.cap = "\\label{fig:gwr} Spatial Variation in GWR Results" }
plotting_data <- map_dfr(c("B","W"), function(race){
  dt <- dt_polygons %>% filter(race == !!race, GEOID %in% nhv_geoids) %>% 
    select(-GEOID, -race) %>% 
    drop_na() %>% st_as_sf() %>% 
    filter(between(value/area, .15, 127)) %>% select(geometry)
  res <- gwr_result[[which(c("B","W") == race)]][[1]]
  renting <- res$SDF$renting
  prop <- res$SDF$prop
  
  bind_cols(dt, renting = renting, prop = prop) %>% 
    mutate(race = ifelse(race == "B", "Black Residents", "White Residents")) %>% 
    drop_na()
}) %>% st_as_sf()

rent <- ggplot(plotting_data, aes(fill = renting)) + 
  geom_sf() + 
  facet_wrap(~race) + 
  scale_fill_viridis() + 
  labs(fill = "Proportion of population who are renters") + 
  theme_map() + 
  theme(legend.position = c(.35,.05), legend.direction = "horizontal",
        legend.key.width = unit(1.2, "cm")) + 
  guides(fill = guide_colorbar(title.position = "top", title.hjust = .5))



prop <- ggplot(plotting_data, aes(fill = prop)) + 
  geom_sf() + 
  facet_wrap(~race) + 
  scale_fill_viridis() + 
  labs(fill = "Proportion of population of this race") + 
  theme_map() + 
  theme(legend.position = c(.35,.05), legend.direction = "horizontal",
        legend.key.width = unit(1.2, "cm")) + 
  guides(fill = guide_colorbar(title.position = "top", title.hjust = .5))


gridExtra::grid.arrange(rent, prop, nrow = 2)

```



# Conclusion 

It is clear from these data that confident conclusions are difficult to draw. We lack information about how  relationships of race and property vary across space, and the coefficients we have are difficult to interpret because we do not have race-specific property data. We have also worked with a limited number of observations overall. Barring this, we have shown racial disparities across key socioeconomic indicators. We have shown significant spatial association of property values in New Haven through Moran's I analysis. We have found that generally, higher proportions of Black residents is associated with lower property values and smaller area at a block group level. The opposite is true for White resident proportions. Many of these conclusions are expected and intuitive. Perhaps more interestingly, we have found preliminary evidence that larger Black households are associated with lower value and smaller area, while the opposite is true for larger White households. In the geographically weighted regression section, we have highlighted trends in specific neighborhoods of New Haven. Long Wharf and Fair Haven neighborhood property values seem to be more sensitive to higher proportions of Black residents, while East Rock and Prospect Hill property values seem to be more associated with the proportion of White households than other neighborhoods.

In this project we have shown disparities that exist in New Haven through statistical analysis and visual maps. These disparities aren't meant to be understood as isolated information solely through the differentiating lens of "race," which can often become nominally vague. There is a complex history in which these disparities were formed, which we attempted to survey in the Introduction. Our results suggest the need for further investigation into the cultural and economic mechanisms that produce disparities, as well as a greater body of evidence to inform policy.









